<?php

if (isset($_POST['Name']) && !empty($_POST['Name']) && isset($_POST['Email']) && !empty($_POST['Email']) && isset($_POST['Phone']) && !empty($_POST['Phone'])) {

  $answer["error"] = false;

  if (!preg_match("~^([a-z0-9_\-\.])+@([a-z0-9_\-\.])+\.([a-z0-9])+$~i", $_REQUEST["Email"]) && $_REQUEST["Email"] != "") {
    $answer["error"] = true;
    $answer["message"] = "Неверный email";
    echo json_encode($answer);
    die();
  }

  $to = "v.sokolov@happylook.ru";

  $doctor = ($_POST['Doctor'])?' к врачу '.$_POST['Doctor']:'';

  $subject = "Запись". $doctor. " с сайта happylook.ru";

  switch ($_POST['action']){
    case 'manufacturing':
      $subject = 'Запись на изготовление очков';
      break;
    case 'diagnostics':
      $subject = 'Запись на диагностику';
      break;
    case 'freelesson':
      $subject = 'Запись на бесплатное занятие: ' . $_POST['disease'];
      break;
    case 'order':
      $subject = "Запись". $doctor. " с сайта happylook.ru";
      break;
    default:
      $subject = 'Запись с сайта happylook.ru';
      break;
  }
  $message = "Пациент <b>" . $_POST['Name'] . "</b><br/>";
  $message .= "Почта: <b>" . $_POST['Email'] . "</b><br/>";
  $message .= "Телефон: <b>" . $_POST['Phone'] . ".</b><br/>";
  $message .= "<p>Комментарий: " . $_POST['Comment'] . ".</p>";

  $header = "From: HappyLook <info@happylook.ru> \r\n";
  //$header .= "Cc:afgh@somedomain.com \r\n";
  $header .= "MIME-Version: 1.0\r\n";
  $header .= "Content-type: text/html\r\n";

  if (!mail($to, $subject, $message, $header)) {
    $answer['error'] = true;
  } else
    $answer["message"] = "Ваше сообщение успешно отправлено!";
} else
  $answer["error"] = true;

echo json_encode($answer);
