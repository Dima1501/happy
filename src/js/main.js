$(document).ready(function () {

    welcomeSvgs();

	popups();

	categoriesHover();

	headerScroll();

	formValidate();

	mask();

	gSlider();

    pSlider();

    dSlider();

    tours();

    pricing();

    brands();

    anchor();

    videos();

    wow();

    tourLazy();

    pChecker();

    tuorsOffsetHide();

    scrollbar();

    $('.preloader').fadeOut();
});

const scrollbar = () => {

    if ($(window).width() > 767) {
        $(".js-scrollbar").mCustomScrollbar();
    } else {
        $('.js-scrollbar').mCustomScrollbar('destroy');
    }

    $(window).on('resize', function () {

        if ($(window).width() > 767) {
            $(".js-scrollbar").mCustomScrollbar();
        } else {
            $('.js-scrollbar').mCustomScrollbar('destroy');
        }
    });
}

const tuorsOffsetHide = () => {

    let clone = $('.tour__scene iframe').clone(true);

    $(window).on('scroll', function () {

        let scrolled = $(window).scrollTop();

        let currentSrc = $('.tour__scene iframe').attr('src');

        if (scrolled + 100 > $('.tour').offset().top + $('.tour').height() || scrolled - 100 + $(window).height() < $('.tour').offset().top) {

            $('.tour__scene iframe').remove();

        } else {

            if ($('.tour__scene iframe').length == 0) {

                $('.tour__scene').append(clone);
                
                let currentSrc = $('.tour__scene iframe').attr('data-src');

                $('.tour__scene iframe').attr('src', currentSrc);
            }
        }
    });
}

const pChecker = () => {

    if ($(window).width() < 768) {
        
        $('.js-drop-opener').on('click', function () {

            $(this).parents('.js-drop-wrapper').find('.js-drop').slideToggle();

            $(this).toggleClass('is-active');

            return false;
        });

        $('.p-checker__link').on('click', function () {

            let name = $(this).text();

            $(this).parents('.p-checker').find('.p-checker__opener').text(name);

            $('.js-drop').slideUp();

            $('.js-drop-opener').removeClass('is-active');
        });

        $('.pricing__side-btn').on('click', function () {

            let name = $(this).text();

            $(this).parents('.pricing').find('.js-drop-opener').text(name);

            $('.js-drop').slideUp();

            $('.js-drop-opener').removeClass('is-active');
        });

        $('.tour__controls-link').on('click', function () {

            let name = $(this).text();

            $(this).parents('.tour').find('.js-drop-opener').text(name);

            $('.js-drop').slideUp();

            $('.js-drop-opener').removeClass('is-active');
        });
    }
}

const tourLazy = () => {

    let bLazy = new Blazy({
        /*breakpoints: [{
            width: 420, src: 'data-small-src'
        }]*/
        success: function(element){
            setTimeout(function(){
                // We want to remove the loader gif now.
                // First we find the parent container
                // then we remove the "loading" class which holds the loader image
                //var parent = element.parentNode;
                //parent.className = parent.className.replace(/\bloading\b/,'');
            }, 200);
        }
    });

    $('.p-slider__video source').each(function () {

        let src = $(this).attr('data-src');

        $(this).attr('src', src);
    });

    $('.s-popup__image').each(function () {

        let src = $(this).attr('data-src');

        $(this).css({
            'background-image': 'url(' + src + ')'
        });
    });

    $('.brands__item-image').each(function () {

        let src = $(this).attr('data-src');

        $(this).attr('src', src);
    });

}

const wow = () => {

    if ($(window).width() > 767) {

        new WOW().init();
    }
}

const welcomeSvgs = () => {

    $('[data-section]').on('mouseenter', function () {

        let needData = $(this).attr('data-section');
        let that = $('.' + needData);

        $('.welcome__sections-item').removeClass('is-active');
        $('#' + needData).addClass('is-active');

        $('.welcome__svg-section').not(that).addClass('hover');
    });

    $('.welcome__svg-section').on('mouseleave', function () {
        $('.welcome__svg-section').removeClass('hover');
        $('.welcome__sections-item').removeClass('is-active');
    });

    $('[data-tour]').on('click', function () {

        let that = $(this)

        let timer = setTimeout(function () {

            let needTour = that.attr('data-tour');

            $('.tour__controls-link').each(function () {

                if ($(this).attr('data-brands') == needTour) {

                    $(this).trigger('click');

                    // $('.tour__more').trigger('click');
                }
            });

        }, 2500);
    });
}




const popups = () => {

	if ($('.popup').length > 0) {

        $('.p-card__link').on('click', function () {

            let index = $(this).parents('.pricing__offers-item').index();
            let popup = $(this).attr('data-popup');

            $('#' + popup).find('.s-popup__num').text('0' + (index + 1));
        });

		$('body').on('click', '[data-popup]', function () {

            var needPopup = $(this).attr('data-popup');

            $('.s-popup').removeClass('is-visible');

            $(this).toggleClass('is-opened');

            $('#' + needPopup).toggleClass('is-visible');

            $('body').toggleClass('is-unscrolled');

            $('.popup__content').fadeIn(0);

            $('.popup__success').removeClass('is-visible');

            if ($(this).hasClass('js-modal')) {
                
                $('.s-popups').addClass('is-opened');
            }

            if ($(this).attr('data-doc')) {
                
                let docName = $(this).attr('data-doc');

                 $('.popup__enemy').fadeIn(0);

                $('.popup__enemy-input').val('К врачу ' + docName);

            } else {
                $('.popup__enemy').fadeOut(0);
            }

            if ($(this).attr('data-issue')) {
                
                let issue = $('.pricing__side-btn.is-active').text();

                 $('.popup__enemy').fadeIn(0);

                $('.popup__enemy-input').val(issue);

            }

            $('.popup__enemy-remove').on('click', function () {

                $('.popup__enemy').fadeOut();
            });

            return false;
        });

        $('body').on('click', '.popup__closer', function () {

            $('.popup').removeClass('is-visible');

            $('body').removeClass('is-unscrolled');

        });

        $('body').on('click', '.s-popup__closer', function () {

            $('.s-popups').removeClass('is-opened');

            $('.s-popup').removeClass('is-visible');

            $('body').removeClass('is-unscrolled');
        });

        $(document).keyup(function(e) {

            if (e.keyCode === 27) {

                $('.popup').removeClass('is-visible');

                $('body').removeClass('is-unscrolled');

                $('.header__drop').removeClass('is-opened');
            }
        });

        $(document).on('click', function (e) {

            var block = $('.popup__container');

            if (!block.is(e.target) &&
                block.has(e.target).length === 0 && $('.popup').hasClass('is-visible')) {

                $('.popup').removeClass('is-visible');

            	$('body').removeClass('is-unscrolled');
            }
        });
	}
}

const categoriesHover = () => {

	$('.header__drop-link').mouseenter(function () {

		let index = $(this).index();

		$('.header__drop-image').removeClass('is-visible');
		$('.header__drop-image').eq(index).addClass('is-visible');

		$('.header__drop-link').removeClass('is-active');
		$(this).addClass('is-active');
	});
}

const headerScroll = () => {

	let lastScrollTop = 0;

    if ($(window).width() > 767) {
        
        $(window).on('scroll', function () {

            let scrolled = $(window).scrollTop();
            let top = window.pageYOffset;

            if (scrolled > 200) {

                $('.header').addClass('header--scrolled');
            } else {
                $('.header').removeClass('header--scrolled');
                $('.header').removeClass('header--show');
            }

            if (lastScrollTop > top && scrolled > 200) {

                $('.header').addClass('header--show');

            } else if (lastScrollTop < top) {

                $('.header').removeClass('header--show');
            }

            lastScrollTop = top;
        });
    }
}

const formValidate = () => {

    if ($('.popup__form').length > 0) {

        var form = $('.popup__form');

        form.each(function () {

            let thatForm = $(this);

            thatForm.validate({
                errorElement: 'span',
                errorClass: 'form__error',

                submitHandler: function () {
                        
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: thatForm.attr('action'),
                        data: thatForm.serialize(),
                        error: function () {
                            console.log("Error messages");
                        },
                        success: function (data) {
                            if (!data.error) {

                                $.when(
                                    $('.popup__content').fadeOut(),
                                    thatForm.find('input[type="text"]').value = '',
                                    thatForm.find('.form__line--error').fadeOut(),
                                    thatForm.find('input').each(function(){
                                        thatForm.blur();
                                    })

                                ).done(function (x) {

                                    $('.popup__success').addClass('is-visible');

                                    setTimeout(function () {

                                        $('.popup').removeClass('is-visible');
                                        $('body').removeClass('is-unscrolled');

                                    }, 4000);
                                });
                            }
                            else {
                                thatForm.find('.form__error_text').text((data.message)?data.message:'');
                                thatForm.find('.form__line--error').fadeIn();
                            }

                        }
                    })
                }
            });
        });

        

        $(form).find('.validate__EMAIL').rules('add', {

            email: true,
            required: true,

            messages: {
                required: 'Введите email',
                email: 'Некорректный email'
            }
        });

        $(form).find('.validate__NAME').rules('add', {

            required: true,

            messages: {
                required: 'Заполните поле',
            }
        });

        $(form).find('.validate__PHONE').rules('add', {

            required: true,

            messages: {
                required: 'Заполните поле',
            }
        });

        $(form).find('.validate__CHECKBOX').rules('add', {

            required: true,

            messages: {
                required: 'Подтвердите согласие на обработку данных',
            }
        });
    }
}

const mask = () => {

	if ($('.phone').length > 0) {

        $(".phone").mask('+7(999)-999-99-99');

    }
}

const gSlider = () => {

	if ($('.g-slider').length > 0) {

		$('.g-slider__scene').each(function () {

			let scene = $(this);

			scene.owlCarousel({
				items: 1,
				dots: true,
				nav: true,
				navText: '',
				navElement: 'a',
                autoHeight: true,
                loop: true,
				navClass: ['g-slider__arrow g-slider__arrow--prev', 'g-slider__arrow g-slider__arrow--next']
			});
		});
	}
}

const pSlider = () => {

    if ($('.p-slider').length > 0) {

        $('.p-slider').each(function () {

            let slider = $(this).find('.p-slider__scene');

            slider.owlCarousel({
                items: 1,
                nav: false,
                dots: false,
                onTranslate: owlChanged,
                onTranslated: owlTranslated,
                onInitialized: owlInitialized
            });

            slider.on('translate.owl.carousel',function(e){
                $('.owl-item video').each(function(){
                    $(this).get(0).pause();
                });
            });

            slider.on('translated.owl.carousel',function(e){
                $('.owl-item.active video').get(0).play();
            });
        });

        function owlInitialized(event) {
            $('.owl-item.active video').get(0).play();
        }

        function owlChanged(event) {

            $('.p-checker__link').removeClass('is-active');
            $('.p-checker__item').eq(event.item.index).find('.p-checker__link').addClass('is-active');
        }

        function owlTranslated(event) {

            let name = $('.p-checker__link.is-active').text();
            $('.p-checker__opener').text(name);
        }

        $('.p-checker__link').on('click', function () {

            let slider = $(this).parents('.persons').find('.p-slider__scene');

            let index = $(this).parents('.p-checker__item').index();

            $('.p-checker__link').removeClass('is-active');

            $(this).addClass('is-active');

            slider.trigger('to.owl.carousel', index);

            return false;
        });
    }
}

const dSlider = () => {

    if ($('.d-slider').length > 0) {

        $('.d-slider__slide').each(function () {

            let index = $(this).index() + 1;

            $(this).attr('data-index', index);
        });

        $('.d-slider').each(function () {

            let slider = $(this).find('.d-slider__scene');

            slider.owlCarousel({
                items: 1,
                nav: true,
                navText: '',
                dots: false,
                onTranslated: owlChanged,
                onInitialized: owlChanged,
                navElement: 'a',
                navClass: ['d-slider__arrow d-slider__arrow--prev', 'd-slider__arrow d-slider__arrow--next'],
                loop: true,
                smartSpeed: 700
            });

            function owlChanged(event) {

                let current = slider.parents('.d-slider').find('.d-slider__counter-current');
                let value = slider.parents('.d-slider').find('.d-slider__counter-value');

                let activeSlideIndex = slider.parents('.d-slider').find('.owl-item.active').find('.d-slider__slide').attr('data-index');

                current.text(activeSlideIndex);
                value.text(event.item.count);
            }
        });
    }
}

const tours = () => {

    $('.tour__more').on('click', function () {

        $(this).toggleClass('is-opened');
        $('.tour__brands.is-active').toggleClass('is-opened');
    });
}

const pricing = () => {

    $('[data-type]').on('click', function () {

        let need = $(this).attr('data-type');

        if ($(this).attr('data-price').length > 0) {

            let price = $(this).attr('data-price');

            $('.pricing').find('.js-price').text(price + ' руб.');
        }

        $('[data-type]').removeClass('is-active');
        $(this).addClass('is-active');
     
        $.when( $(".pricing__offers-section").fadeOut(200) ).then(function(){ 
            $('#'+need).fadeIn(200);
        });

        return false;
    });
}

const brands = () => {

    $('[data-brands]').on('click', function () {

        $('.tour__more').removeClass('is-opened');

        $('.tour__brands').removeClass('is-opened');

        let category = $(this).attr('data-brands');

        $('.tour__controls-link').removeClass('is-active');

        $(this).addClass('is-active');

        let scene = $(this).attr('data-scene');

        $('.tour__scene').css('background', 'none');

        $('.tour__content').attr('src', scene);

        $('.tour__brands').removeClass('is-active');

        $('#' + category).addClass('is-active');

        return false;
    });
}

const anchor = () => {

    $('[data-anchor]').on('click', function () {

        let index = $(this).attr('data-anchor');

        let offset = $('#' + index).offset().top;

        $('html, body').animate({
            scrollTop: offset - 50
        }, 700);

        $('.header__drop').removeClass('is-visible');
        $('.header__menu').removeClass('is-opened');
        $('body').removeClass('is-unscrolled');

        return false;
    });
}

const videos = () => {

    if ($('.js-youtube').length > 0) {

        $('.js-youtube').YouTubePopUp();
    }
}














