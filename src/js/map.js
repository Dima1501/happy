$(window).on('load', function () {

    if ( $('#yaMap').length > 0 ) {

        ymaps.ready(initYaMap);
    }
});


function initYaMap() {

    function getZoom() {

        if ($(window).width() > 1279) {
            return 16;
        }

        if ($(window).width() < 1280 && $(window).width() > 767) {
            return 15;
        }

        if ($(window).width() < 768) {
            return 14;
        }
    }

    function getMarkerSize() {

        if ($(window).width() < 768) {
            return [35, 40]
        } else {
            return [55, 80]
        }
    }

    function getMarkerOffset() {

        if ($(window).width() < 768) {
            return [-17, -40]
        } else {
            return [-22, -80]
        }
    }

	var myMap = new ymaps.Map('yaMap', {
            center: [59.92818216, 30.35662057],
            zoom: getZoom(),
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),

        myPlacemark = new ymaps.Placemark([59.92502797, 30.35788182], {
        }, {
            // Опции.
            iconLayout: 'default#image',
            iconImageHref: 'assets/images/static/map-marker.svg',
            iconImageSize: getMarkerSize(),
            iconImageOffset: getMarkerOffset()
        });

    myMap.geoObjects
        .add(myPlacemark);
}
